import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TextAnalysis {
    private void informationOnText(String text) {
        String words = text.replaceAll("[^(А-Яа-яA-Za-z0-9)\\s+]", "");
        String[] wordsResult = words.split("[\\s]");
        Map<String, Integer> mapWords = new HashMap<>();
        for (String word : wordsResult) {
            mapWords.put(word, mapWords.getOrDefault(word, 0) + 1);
        }
        mapWords.forEach((k, v) -> System.out.println("Слово: " + "|" + k + "|" + " всречается: " + v + " раз/а"));
        System.out.println("Всего слов в тексте: " + wordsResult.length);
    }
    public static void main(String[] args) {
        TextAnalysis textAnalysis = new TextAnalysis();
        String text1 = "Генерал-поручик!.. Он у меня в роте был сержантом!.. Обоих российских орденов кавалер!..эскадрилья ";
        textAnalysis.informationOnText(text1);
    }
}




